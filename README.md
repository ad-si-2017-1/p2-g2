<h1 align="center">Projeto 2 - Grupo 2</h1>
<h1 align="center">Chat - WebSocket</h1>

<h3>Integrantes</h3>
- Bruno Pidde
- Newton Junior
- Rhandy Mendes
- Felipe Neves
- Rhuan Bruno
- Debora Santos

<h3><b><i>Sobre as tecnologias usadas no Projeto</i></b></h3>

A especificação de WebSocket define uma API que estabelece "socket" conexões entre um navegador da web e um servidor. Em palavras simples: há uma conexão persistente entre o cliente e o servidor e ambas as partes podem começar a enviar dados a qualquer momento.

<h3>Sobre o WebSocket:</h3>
Este é o novo esquema de URL para conexões de WebSockets. Há também o wss que serve para fixar o WebSocket conexão a mesma maneira https é usado para conexões HTTP seguras.
Anexar manipuladores de eventos alguns imediatamente para a conexão permite que você saiba quando a conexão é aberta, recebeu as mensagens recebidas, ou há um erro.
O segundo argumento aceita subprotocolos opcionais. Pode ser uma sequência de caracteres ou uma matriz de sequências de caracteres. Cada sequência de caracteres deve representar um nome subprotocolo e o servidor aceita apenas um subprotocolos  anterior na matriz. Subprotocolo aceito pode ser determinado, acessando a propriedade protocolo WebSocket objeto.

<h3>Comunicação com o servidor</h3>
Quando temos uma conexão para o servidor podemos começar a enviar dados para o servidor usando o send método no objeto de conexão. Costumava apoiar apenas sequências de caracteres, mas na especificação mais recente ele agora pode enviar mensagens de binárias também.Igualmente o servidor pode enviar-nos mensagens a qualquer momento. Sempre que isso acontece os onmessage fogos de retorno de chamada. O retorno de chamada recebe um objeto de evento e a mensagem real é acessível através da propriedade de dados. WebSockets também podem receber mensagens de binárias nas especs mais recente binário quadros podem ser recebidos em formato Blob ou ArrayBuffer. Para especificar o formato do binário recebido, defina a propriedade binaryType do objeto WebSocket 'blob' ou 'arraybuffer'. O formato padrão é 'blob'.

<h3>Servidores proxy</h3>
Cada nova tecnologia vem com um novo conjunto de problemas. No caso de WebSocket é a compatibilidade com servidores proxy que mediam as conexões HTTP na maioria das redes da empresa. O protocolo WebSocket usa o HTTP atualização sistema (que é normalmente usado para HTTP/SSL) para "upgrade" uma conexão HTTP para uma conexão de WebSockets. Alguns servidores de proxy não gosto disso e vão cair a conexão. Assim, mesmo se um determinado cliente usa o protocolo WebSocket, pode não ser possível estabelecer uma conexão. Isso faz com que a próxima seção ainda mais importante.

<h3>O uso de WebSockets hoje</h3>
Websockets ainda é uma tecnologia nova e não totalmente implementada em todos os navegadores. No entanto, você pode usar WebSocket hoje com bibliotecas que usam uma das restaurações mencionadas acima sempre que WebSocket não está disponível. Uma biblioteca que se tornou muito popular neste domínio é socket.io que vem com um cliente e uma implementação de servidor do protocolo e inclui restaurações. Também existem soluções comerciais tais como PusherApp, que pode ser facilmente integrado em qualquer ambiente web, fornecendo uma API de HTTP para enviar mensagens de Websockets para clientes. Devido ao pedido HTTP extra sempre haverá extra sobrecarga em comparação com WebSockets puro.

<h3>No lado do servidor</h3>
Usar Websockets cria um novo padrão de uso para o servidor de aplicativos do lado. Enquanto servidor tradicional pilhas tais como lâmpada são projetados em torno do ciclo de solicitação/resposta HTTP frequentemente não lidam bem com um grande número de conexões abertas WebSockets. Mantendo um grande número de conexões abertas ao mesmo tempo exige uma arquitetura que recebe alta concorrência, a um custo baixo desempenho. Essas arquiteturas são geralmente concebidas em torno de qualquer segmentação ou então chamadas IO sem bloqueio.

<h3>Versões do protocolo</h3>
O protocolo base para WebSocket é o RFC6455. As últimas versões Chrome e Chrome para Android são totalmente compatíveis com RFC6455 incluindo mensagens de binário. Além disso, o Firefox é compatível na versão 11, Internet Explorer na versão 10. Você ainda pode usar as versões mais antigas do protocolo, mas não é recomendado já que eles são conhecidos por serem vulneráveis. Se você tem implementações de servidor para versões mais antigas do protocolo WebSocket, recomendamos que faça o upgrade para a versão mais recente.

<h3>Referencia</h3>
https://www.html5rocks.com/en
 

 

var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var irc = require('irc');
var express = require("express");
app.use(express.static(__dirname));

app.get('/', function (req, res) {
	res.sendFile('/index.html');
	res.send('O Servidor esta rodando');
});

io.on("connection", function (client) {

	client.on("join", function (name, channel) {

		var clientIrc = new irc.Client('irc.freenode.net', name, {
			autoConnect: false

		});

		clientIrc.connect(5, function (name) {
			console.log("Conectado");
			clientIrc.join(channel, function (name) {
				console.log(name);
				console.log("/join " + channel);
				clientIrc.say(channel, "Conectado");
				console.log("Joined do " + name + " efetuado");

			});

		});
	
		clientIrc.addListener('message', function (from, to, text) {
			console.log(from + ' => ' + to + ': ' + text);
			console.log("Message: " + text);
			client.emit('chat', from, text);
		});

		clientIrc.addListener('names', function (channel, nicks) {
			client.emit("update", "", nicks);
			console.log(nicks);
		});
		
		clientIrc.addListener('join', function (channel, nick) {
			client.emit("update", nick);
		});

		clientIrc.addListener('quit', function (nick, reason, channels, message) {
			console.log("quit");
		});

		clientIrc.addListener('message#', function (nick, to, text, message) {
			console.log("message#");
		});

		clientIrc.addListener('error', function (message) {
			console.log("Erro");
		});

		
		clientIrc.addListener('nick', function (oldNick,newNick) {
			var mensagem="O usuario "+oldNick+" trocou de nome para "+newNick;
			
			console.log(mensagem);
			client.emit("update", mensagem);
		});

		client.on("disconnect", function () {
			clientIrc.disconnect();
			console.log("Disconnect");
		});

		client.on("send", function (msg) {
			var mensagem = String(msg).trim();
			var args = mensagem.split(" ");
			var comando=args[0].toUpperCase();
			if (comando === "/NICK") nick(args);
			else if (comando === "/NAMES") names(args);
			else {
				clientIrc.say(channel, msg);
			}
		});
		function nick(args) {
			clientIrc.send('nick', args[1]);
		}
		function names(args) {
			if(args[1] !=null){
			 clientIrc.send('names', args[1]);
			}
		}
	});

});

http.listen(3000, function () {
	console.log('Conectado na porta 3000');
	console.log('Favor acessar o endereco localhost:3000 no seu browser de preferencia!');
});


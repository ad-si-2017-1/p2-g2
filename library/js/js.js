$(document).ready(function () {
	var socket = io.connect("http://localhost:3000");
	var ready = false;

	$("#submit").submit(function (e) {
		e.preventDefault();
		$("#nick").fadeOut();
		$("#chat").fadeIn();
		var name = $("#nickname").val();
		var channel = $("#channel").val();
		$("#name").html(name);
		$("#canal").html(channel);

		socket.emit("join", name, channel);
		ready = true;
	});

	$("#textarea").keypress(function (e) {
		if (e.which == 13) {
			sendMessage();
		}
	});

	socket.on("update", function (msg, clients) {
		if (ready) {

			if (msg !== "") {
				var args = msg;
				var args = args.split(" ");
				var note = null;
				if (args[0] === "nick") {
					var newNote = msg.replace("nick", "");
					console.log(newNote);
					note=newNote;
				} else if (args[0] != "nick"){
					if (msg === $("#nickname").val()) {
						note = "-->> Bem vindo a sala de Bate Papo de AD. <<--";
						msg == "";
					} else {
						note = msg + " << -- acabou de entrar na sala.";
						msg == "";
					}
				}

				$('.chat').append('<li style="margin-bottom: 4px;" class="info"> ' + note + ' </li>')
			}

		}
	});

	socket.on("chat", function (client, msg) {
		if (ready) {
			var time = new Date();
			$(".chat").append('<div class="row"><div class="receiver"><div class="message-text"><li><div class="msg"><span>' + client + ':</span><p>' + msg + '</p> <span class="message-time pull-right"> </div></div></li></div></div></div>');
			$("#chat").animate({ scrollTop: $('#chat').prop("scrollHeight") }, 500);
			$("#conversation").scrollTop($("#conversation").scrollTop() + $("#conversation").innerHeight());
		}
	});

	function sendMessage() {
		var text = $("#textarea").val();
		if (isNotNull(text)) {
			var text = $("#textarea").val();
			$("#textarea").val('');
			var time = new Date();
			$(".chat").append('<div class="row message-body"> <div class="col-sm-12message-main-sender"><div class="sender"><div class="message-text"><li><div class="msg"><span> Eu: </span><p>' + text + '</p> <span class="message-time pull-right"> </div></div></li></div></div></div></div>');
			$("#conversation").scrollTop($("#conversation").scrollTop() + $("#conversation").innerHeight());
			socket.emit("send", text);
		}
	};

	function isNotNull(x) {
		if (x != "" && !(x.match(/^\s+$/))) {
			return true;
		} else {
			return false;
		}
	};

});





